<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<div id="wrap">
		<div class="result">
			<p id="d"></p>
		</div>
		<div class="content">
			<div class="row">
				<input type="button" value="7" class="btnwhite" onclick='cal("7")'>
				<input type="button" value="8" class="btnwhite" onclick='cal("8")'>
				<input type="button" value="9" class="btnwhite" onclick='cal("9")'>
				<input type="button" value="/" class="btngreen" onclick='cal("/")'>	
			</div>
			<div class="row">
				<input type="button" value="4" class="btnwhite" onclick='cal("4")'>
				<input type="button" value="5" class="btnwhite" onclick='cal("5")'>
				<input type="button" value="6" class="btnwhite" onclick='cal("6")'>
				<input type="button" value="*" class="btngreen" onclick='cal("*")'>	
			</div>
			<div class="row">
				<input type="button" value="1" class="btnwhite" onclick='cal("1")'>
				<input type="button" value="2" class="btnwhite" onclick='cal("2")'>
				<input type="button" value="3" class="btnwhite" onclick='cal("3")'>
				<input type="button" value="-" class="btngreen" onclick='cal("-")'>	
			</div>
			<div class="row">
				<input type="button" value="0" class="btnwhite" onclick='cal("0")'>
				<input type="button" value="." class="btnwhite" onclick='cal(".")'>
				<input type="button" value="=" class="btnwhite" onclick='result()'>
				<input type="button" value="+" class="btngreen" onclick='cal("+")'>	
			</div>
			<input type="button" value="C" class="btnred" style="float:right;" onclick='c("")'>
		</div>
	</div>
	
	<script>
		function c(val){
			document.getElementById("d").innerHTML = val;
		}
		function cal(val){
			document.getElementById("d").innerHTML += val;
		}
		function result(){
			c(eval(document.getElementById("d").innerHTML));
		}
	</script>
</body>
</html>